#include <sys/types.h>
#include <sys/socket.h>
#include <sys/uio.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>
#include <netdb.h>
#include <signal.h>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <ctime>

struct req_data {
	const char *method;
	const char *path;
	const char *qry_str;
	int content_length;
	in_addr addr;
};
const char *document_root;
char *get_file(const char *file_addr, int &length);
void exec_cgi(int fd, req_data &req);
void accept_client(int fd, in_addr &addr);
void set_server_env(const char *server_port);
void set_request_env(req_data &data);
FILE *log_file;
int s_fd;
char *env_vars[100];

int main(int argc, char *argv[]) {
	if (argc < 3) {
		fprintf(stderr, "Usage: %s <port> <document_root> [log_file]\n", argv[0]);
		exit(1);
	}

	document_root = argv[2];
	set_server_env(argv[1]);
	signal(SIGCHLD, SIG_IGN);

	if (!(argc == 4 && (log_file = fopen(argv[3], "a")))) {
		puts("Log on stdout.");
		log_file = stdout;
	}

	// Socket, bind, and listen
	struct sockaddr_in addr;
	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(atoi(argv[1]));
	addr.sin_addr.s_addr = htons(INADDR_ANY);

	s_fd = socket(AF_INET, SOCK_STREAM, 0);
	if(s_fd < 0 || bind(s_fd, (struct sockaddr *)&addr, sizeof(addr)) < 0 || listen(s_fd, 1000) < 0) {
		puts("Fail to bind or listen, check netstat or change a port.");
		exit(-1);
	}

	// Accept and create a thread for each client
	while(1) {
		sockaddr_in caddr;
		socklen_t len = sizeof(sockaddr_in);

		int client_fd = accept(s_fd, (sockaddr *) &caddr, &len);
		accept_client(client_fd, caddr.sin_addr);
		close(client_fd);
	}
}

void accept_client(int fd, in_addr &addr) {
	// Receive from browser
	char recv_msg[65536] = {0};
	if(read(fd, recv_msg, 65535) <= 0) {
		return;
	}

	// Write log
	time_t nowtime = time(NULL);
	char *time_str = ctime(&nowtime);
	time_str[strlen(time_str) - 1] = '\0';
	fprintf(log_file, "\e[1;32m[%s]Receive > \e[m\r\n%s\r\n", time_str, recv_msg);
	fflush(log_file);

	// Split command and path then do what should be done
	req_data req;
	req.content_length = 0;
	req.addr = addr;

	req.method = strtok(recv_msg, " ");
	char *path = strtok(NULL, " ");
	req.path = strtok(path, "?");
	req.qry_str = strtok(NULL, " ");
	if (req.qry_str == NULL)
		req.qry_str = "";

	// Get real file address
	char real_path[strlen(document_root) + strlen(req.path) + 1];
	sprintf(real_path, "%s%s", document_root, req.path);

	// For non-supported command
	if(strcmp(req.method, "GET") != 0) {
		write(fd, "HTTP/1.0 400 Bad Request\r\n", 26);
	
	// For CGI request
	} else if (strlen(real_path) > 4 && !strcmp(real_path + strlen(real_path) - 4, ".cgi")) {
		struct stat sb;

		if (stat(real_path, &sb) != 0 || (sb.st_mode & S_IFMT) != S_IFREG) {
			write(fd, "HTTP/1.0 404 Not Found\r\n\r\n", 26);
		} else if (!sb.st_mode & S_IRUSR) {
			write(fd, "HTTP/1.0 403 Forbidden\r\n\r\n", 26);
		} else if (sb.st_mode & S_IXUSR) {
			write(fd, "HTTP/1.0 200 OK\r\n", 17);
			exec_cgi(fd, req);
		} else {
			write(fd, "HTTP/1.0 500 Internal Server Error\r\n\r\n", 38);
		}

	// For normal file request
	} else {
		int length;
		char *response = get_file(real_path, length);

		if (response) {
			char buf[120];
			int header_length = sprintf(buf, "HTTP/1.0 200 OK\r\nContent-Length: %d\r\nContent-Type: %s\r\n\r\n", length, "text/html");
			write(fd, buf, header_length);
			write(fd, response, length);
			free(response);
		} else {
			write(fd, "HTTP/1.0 404 Not Found\r\n\r\n", 27);
		}
	}
}

char *get_file(const char *file_addr, int &length) {
	// Open file and response if no such file
	FILE *file_ptr = fopen(file_addr, "rb");
	if(file_ptr == NULL) {
		return NULL;
	}

	// Get file's length
	fseek(file_ptr, 0, SEEK_END);
	length = ftell(file_ptr);
	rewind(file_ptr);

	// Make response message
	char *response = (char *) malloc(length);
	if(response == NULL)
		exit(-1);

	fread(response, 1, length, file_ptr);

	fclose(file_ptr);
	return response;
}

void exec_cgi(int fd, req_data &req) {
	if (fork() == 0) {
		chdir(document_root);
		set_request_env(req);

		dup2(fd, STDIN_FILENO);
		dup2(fd, STDOUT_FILENO);
		dup2(fd, STDERR_FILENO);
		close(s_fd);
		close(fd);

		execle(req.path + 1, req.path, NULL, env_vars);
		exit(2);
	}
}

void set_server_env(const char *server_port) {
	char hostname[100];
	gethostname(hostname, 100);

	env_vars[0] = "SERVER_SOFTWARE=NP_project3";
	env_vars[1] = "GATEWAY_INTERFACE=CGI/1.1";
	env_vars[2] = "SERVER_PROTOCOL=HTTP/1.0";
	env_vars[3] = (char *) malloc(15 + strlen(hostname));
	sprintf(env_vars[3], "SERVER_NAME=%s", hostname);
	env_vars[4] = (char *) malloc(15 + strlen(server_port));
	sprintf(env_vars[4], "SERVER_PORT=%s", server_port);
}

void set_request_env(req_data &data) {
	env_vars[5] = (char *) malloc(20 + strlen(data.method));
	sprintf(env_vars[5], "REQUEST_METHOD=%s", data.method);
	env_vars[6] = (char *) malloc(20 + strlen(data.path));
	sprintf(env_vars[6], "SCRIPT_NAME=%s", data.path);
	env_vars[7] = (char *) malloc(20 + strlen(data.qry_str));
	sprintf(env_vars[7], "QUERY_STRING=%s", data.qry_str);

	env_vars[8] = (char *) malloc(40);
	sprintf(env_vars[8], "CONTENT_LENGTH=%d", data.content_length);

	hostent *h = gethostbyaddr(&data.addr, sizeof(data.addr), AF_INET);
	env_vars[9] = (char *) malloc(15 + strlen(h->h_name));
	sprintf(env_vars[9], "REMOTE_HOST=%s", h->h_name);
	env_vars[10] = (char *) malloc(40);
	sprintf(env_vars[10], "REMOTE_ADDR=%s", inet_ntoa(*(in_addr *) h->h_addr));

	/* These variables are supposed to unset. Just following the instruction on the spec. */
	env_vars[11] = "AUTH_TYPE=Anonymous";
	env_vars[12] = "REMOTE_USER=Anonymous";
	env_vars[13] = "REMOTE_IDENT=Not_looking_up";
	env_vars[14] = NULL;
}
