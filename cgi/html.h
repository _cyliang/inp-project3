#pragma once

void printHeader(int colCount, const char *title[]);
void printCol(int col, const char *str, bool strong = false);
void printFooter();
