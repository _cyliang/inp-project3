#include "connection.h"
#include "html.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <cstring>

Connection::Connection(JobList &_jobList, int _id, const char *host, unsigned short port, const char *_file):
	id(_id), jobList(_jobList), file(_file, std::ios::in) {
	
	if (!file) {
		print("Cannot find the file.");
		return;
	}

	hostent *ent = gethostbyname(host);
	if (!ent) {
		print("Cannot find host.");
		return;
	}

	memset(&addr, 0x00, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	memcpy(&addr.sin_addr, ent->h_addr, ent->h_length);

	sockFd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockFd == -1) {
		print("Cannot create socket.");
		return;
	}

	int fflag = fcntl(sockFd, F_GETFL, 0);
	fcntl(sockFd, F_SETFL, fflag | O_NONBLOCK);

	pushConnect();
}

void Connection::pushConnect() {
	int result = connect(sockFd, (sockaddr *) &addr, sizeof(addr));
	if (result == 0 || errno == EISCONN) {
		pushRead();
	} else {
		switch (errno) {
			case EALREADY:
			case EINPROGRESS:
				jobList.pushWriteJob(sockFd, NULL, 0, pushConnectCallback, this);
				break;

			default:
				print(strerror(errno));
		}
	}
}

void Connection::pushConnectCallback(void *arg, ssize_t) {
	Connection *_this = (Connection *) arg;
	_this->pushConnect();
}

void Connection::pushWrite() {
	if (file) {
		getline(file, fileLine);
		fileLine.push_back('\n');

		print(fileLine.c_str(), true);
		jobList.pushWriteJob(sockFd, (void *) fileLine.c_str(), fileLine.length(), pushWriteCallback, this);
	} else {
		file.close();
	}
}

void Connection::pushWriteCallback(void *arg, ssize_t) {
	Connection *_this = (Connection *) arg;
	_this->pushRead();
}

void Connection::pushRead() {
	jobList.pushReadJob(sockFd, readBuf, sizeof(readBuf) - 1, pushReadCallback, this);
}

void Connection::pushReadCallback(void *arg, ssize_t n) {
	Connection *_this = (Connection *) arg;

	if (n) {
		_this->readBuf[n] = '\0';
		_this->print(_this->readBuf);

		if (!strcmp(_this->readBuf + n-2, "% "))
			_this->pushWrite();
		else
			_this->pushRead();
	} else {
		close(_this->sockFd);
	}
}

void Connection::print(const char *str, bool strong) {
	::printCol(id, str, strong);
}
