#include "html.h"
#include <iostream>
#include <cstring>
using namespace std;

void printHeader(int colCount, const char *title[]) {
	cout << "Content-type: text/html\r\n\r\n" << flush;

	cout << 
		"<!DOCTYPE html>\n"
		"<html>\n"
		"<head>\n"
		"	<title>Network Programming Homework 3</title>\n"
		"	<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css\" integrity=\"sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7\" crossorigin=\"anonymous\">\n"
		"	<script src=\"http://code.jquery.com/jquery-1.11.3.min.js\"></script>\n"
		"	<style>\n"
		"		.column {\n"
		"			min-width: 400px;\n"
		"		}\n"
		"\n"
		"		#container {\n"
		"			min-width: " << colCount * 400 << "px;\n"
		"		}\n"
		"	</style>\n"
		"</head>\n"
		"\n"
		"<body>\n"
		"	<div class=\"jumbotron\">\n"
		"		<div class=\"container-fluid\">\n"
		"			<h1>Network Programming Homework3</h1>\n"
		"			<h2>CGI</h2>\n"
		"		</div>\n"
		"	</div>\n"
		"	<div class=\"container-fluid\" id=\"container\">\n"
		"		<div class=\"row\">\n";
	
	for (int i=0; i<colCount; i++) {
		cout <<
			"			<div class=\"column col-sm-" << (12 / colCount) << "\">\n"
			"				<h3>" << title[i] << "</h3>\n"
			"				<pre id=\"col" << i << "\"></pre>\n"
			"			</div>\n";
	}

	cout <<
		"		</div>\n"
		"	</div>" << endl;
}

void printCol(int col, const char *str, bool strong) {
	cout << "<script>$('" << (strong ? "<strong>" : "<span>") << "').appendTo('#col" << col << "').text(\"";

	int pos = 0;
	int len = strlen(str);
	while (pos != len) {
		switch (str[pos]) {
			case '\n':
				cout << "\\n";
				break;
			case '\r':
				break;
			case '"':
				cout << "\\\"";
				break;
			case '\\':
				cout << "\\\\";
				break;
			default:
				cout << str[pos];
		}

		++pos;
	}

	cout << "\")</script>" << endl;
}

void printFooter() {
	cout << 
		"\n"
		"</body>\n"
		"</html>" << endl;
}
