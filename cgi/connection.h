#pragma once
#include "job.h"
#include <netinet/in.h>
#include <fstream>
#include <string>

class Connection {
public:
	Connection(JobList &jobList, int id, const char *host, unsigned short port, const char *file);

private:
	void pushConnect();
	static void pushConnectCallback(void *, ssize_t);
	void pushWrite();
	static void pushWriteCallback(void *, ssize_t);
	void pushRead();
	static void pushReadCallback(void *, ssize_t);
	void print(const char *, bool strong = false);

	const int id;
	JobList &jobList;
	int sockFd;
	sockaddr_in addr;
	std::ifstream file;
	std::string fileLine;
	char readBuf[10000];
};
