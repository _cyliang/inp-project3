#include "html.h"
#include "connection.h"
#include "job.h"
#include <cstdlib>
#include <cstring>
#include <cctype>
#include <cstdio>
#include <iostream>
using namespace std;

#define MAX_QUERY_NUM 200

void urldecode(char *dst, const char *src) {
	int len = strlen(src);
	int i, j;
	for (i=0, j=0; i<len; i++, j++) {
		char &d = dst[j];

		switch (src[i]) {
			case '%':
				if (isxdigit(src[i+1]) && isxdigit(src[i+2])) {
					int ascii;
					sscanf(src + i+1, "%2x", &ascii);
					d = (char) ascii;
					i += 2;
				} else {
					d = '%';
				}
				break;
			case '+':
				d = ' ';
				break;
			default:
				d = src[i];
		}
	}

	dst[j] = '\0';
}

int main() {
	const char *qry_str = getenv("QUERY_STRING");
	if (!qry_str) {
		cerr << "This CGI should be executed by web server." << endl;
		exit(2);
	}

	struct ServerInfo {
		const char *host;
		const char *port;
		const char *file;
	} servers[MAX_QUERY_NUM] = {0};
	int server_count = 0;

	char qry[strlen(qry_str) + 1];
	strcpy(qry, qry_str);

	char *pos = strtok(qry, "&");
	while (pos) {
		int server_no = -1;
		sscanf(pos + 1, "%d", &server_no);
		
		if (server_no > 0 && server_no < MAX_QUERY_NUM) {
			ServerInfo &s = servers[server_no];
			char ent = pos[0];
			pos = strchr(pos, '=') + 1;

			if (strlen(pos)) {
				switch (ent) {
					case 'h':
						s.host = pos;
						++server_count;
						break;
					case 'p':
						s.port = pos;
						break;
					case 'f':
						s.file = pos;
						break;
				}
			}
		}
		
		pos = strtok(NULL, "&");
	}

	ServerInfo *servers_reduced[server_count];
	const char *titles[server_count];
	for (int i=0, s=0; i<MAX_QUERY_NUM && s<server_count; i++) {
		if (servers[i].host) {
			titles[s] = servers[i].host;
			servers_reduced[s++] = &servers[i];
		}
	}
	printHeader(server_count, titles);

	JobList jobList;
	Connection *connections[server_count];
	for (int i=0; i<server_count; i++) {
		ServerInfo &server = *servers_reduced[i];
		char file_name[strlen(server.file) + 1];
		urldecode(file_name, server.file);

		connections[i] = new Connection(
				jobList, i,
				server.host,
				(unsigned short) atoi(server.port),
				file_name
		);
	}

	jobList.loop();
	printFooter();
}
